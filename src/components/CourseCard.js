// import { useState, useEffect } from 'react';
import {Row, Col, Card, Button}  from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}) {

    // console.log(props)
    // console.log(typeof props)
    // console.log(props.courseProp.name)
    // console.log(courseProp.name)

/*
    USE the state hook for this component to be able to store its state
    states are used to keep track of informatiion related to individual components

    syntax:
        const [getter, setter] = useState(initialGetterValue)
*/

    // const [count, setCount] = useState(0)
    // const [seats, setSeats] = useState(30)

    // function enroll() {
    //    // if (seats > 0) {
    //         setCount(count + 1);
    //         console.log('Enrollees: ' + count);
    //         setSeats(seats - 1);
    //         console.log('Seats: ' + seats);
    //    //   } else {
    //    //      alert("No more seats available");
    //    // }; 

    // }

    // useEffect(() => {

    //     if(seats === 0){
    //         alert('No more seats available')
            
    //     }

    // }, [seats])



    const { name, description, price, _id } = courseProp;
    console.log(courseProp)

    return(
        <Row>
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Title>PRICE</Card.Title>
                    <Card.Text>PHP {price}</Card.Text>
                    <Button variant="primary" as={Link} to ={`/courses/${_id}`}>Details</Button>
                    {/*<Link className="btn btn-primary"  to="/courseview">Details</Link>*/}
                    </Card.Body>
                </Card>
            </Col>
        </Row>

        )
}