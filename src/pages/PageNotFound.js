import { Link } from 'react-router-dom';
import {Button, Row, Col} from 'react-bootstrap';

export default function PageNotFound() {
		return(

			<Row>
				<Col className="p-5">
					<h1>Page Not Found</h1>
					<p>Go back to the <Button variant="primary" as={Link} to="/">homepage</Button></p>
				</Col>
			</Row>
		)
}
